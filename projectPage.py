"""
alena hemminger
make system guess user input number using while and random
"""
# import library with random capabilities
import random as rand

# create while and signal in order to run program again if user wants to
repeat_signal = 0
while repeat_signal == 0:
    print("How many pieces of popcorn do you think are in a bag of microwavable popcorn?")  # avg is 300
    popcorn = int(input())

    # make it so that the user's input must be within the random range's capabilities created later
    if popcorn > 1000:
        print("That is not a good guess, try something lower")
        popcorn = int(input())
    elif popcorn <= 0:
        print("That is not a good guess, try something higher")
        popcorn = int(input())

    # get user input for fudge room
    print("How many pieces off do you allow the computer's guess to be off? (fudge room)")
    error_room = int(input())

    comp_guess = 0
    guess_count = 0

    # if the computer's "guess" is not within error_room pieces of the user's guess,
    # continue guessing until within error_room then exit while loop
    while abs(popcorn - comp_guess) >= error_room:
        guess_count = guess_count + 1
        comp_guess = rand.randrange(0, 1001)
        print("Guess", guess_count, ":", comp_guess)

    # print after loop finishes
    print("********************")
    print("You guessed that the average amount of kernels in a bag of popcorn is", popcorn)
    print("The computer guessed", comp_guess, ", which is", abs(popcorn - comp_guess),
          "pieces away from your guess, in", guess_count, "tries")
    print("Did you know the average amount of pieces of popcorn in a microwave bag is around 300?")
    print("********************")

    # rerun or end program based on user input
    print("To rerun guessing, press the r key. Press any other key to end program.")
    rerun = input()
    if rerun == "r":
        repeat_signal = 0
    else:
        repeat_signal = 1

print("End of guessing")