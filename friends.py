# create variable to store points
"""
point system:
15 - things very important to me
10 - important
5 - not too important
"""
comp_score = 0
max_points = 75
print("Might we be friends?")
name = input("What is your name? ")
print("You said", name, "Nice to meet you,", name)
print("Let's find out if we should be friends!")

# question 1: cats
print("Q1: Do you like cats (0=no, 1=yes)")
# make sure they respond with number
q1_signal = 1
while q1_signal == 1:
    # noinspection PyBroadException
    try:
        resp_cats = float(input())
        q1_signal = 0
    except:
        print("Invalid response. Please type 0 or 1 (0=no, 1=yes)")

if resp_cats == 0:
    pass
else:
    comp_score = comp_score + 15
    print("Me too! My cat's name is Fiona. Do you have a cat? If yes, what is/are their name(s)?")
    cat_name = input()
    if cat_name == "no" or cat_name == "No":
        print("I'm sorry :(")
    else:
        print("You said", cat_name, "That's a cute name!")

# question 2: weightlifting
print("Q2: Do you weightlift regularly? (0=no, 1=yes)")
# make sure they respond with number
q2_signal = 1
while q2_signal == 1:
    # noinspection PyBroadException
    try:
        resp_weight = float(input())
        q2_signal = 0
    except:
        print("Invalid response. Please type 0 or 1 (0=no, 1=yes)")

if resp_weight == 0:
    pass
else:
    comp_score = comp_score + 10
    print("Same here! My favorite day is chest day. What's yours?")
    fav_day = input()
    print("You said", fav_day, "Good for you!")

# question 3: video games
print("Q3: Do you play video games? (0=no, 1=yes)")
# make sure they respond with number
q3_signal = 1
while q3_signal == 1:
    # noinspection PyBroadException
    try:
        resp_game = float(input())
        q3_signal = 0
    except:
        print("Invalid response. Please type 0 or 1 (0=no, 1=yes)")

if resp_game == 0:
    pass
else:
    comp_score = comp_score + 5
    print("Awesome! I play Rocket League for Carlow's Esports team. What's your favorite game?")
    fav_game = input()
    print("You said", fav_game, "Good for you!")

# question 4: cleanliness
print("Q4: Do you keep yourself and your living space clean? (0=no, 1=yes)")
# make sure they respond with number
q4_signal = 1
while q4_signal == 1:
    # noinspection PyBroadException
    try:
        resp_clean = float(input())
        q4_signal = 0
    except:
        print("Invalid response. Please type 0 or 1 (0=no, 1=yes)")

if resp_clean == 0:
    pass
else:
    comp_score = comp_score + 15
    print("Thank goodness!")

# question 5:
print("Q5: Do you have a good sense of humor? (0=no, 1=yes)")
# make sure they respond with number
q5_signal = 1
while q5_signal == 1:
    # noinspection PyBroadException
    try:
        resp_humor = float(input())
        q5_signal = 0
    except:
        print("Invalid response. Please type 0 or 1 (0=no, 1=yes)")

if resp_humor == 0:
    pass
else:
    comp_score = comp_score + 15

print("Q6: Do you like watching football? (0=no, 1=yes)")
# make sure they respond with number
q6_signal = 1
while q6_signal == 1:
    # noinspection PyBroadException
    try:
        resp_football = float(input())
        q6_signal = 0
    except:
        print("Invalid response. Please type 0 or 1 (0=no, 1=yes)")

if resp_football == 0:
    pass
else:
    comp_score = comp_score + 5
    print("Awesome! Do you play fantasy football? (0=no, 1=yes)")
    # nested conditional
    q6_1_signal = 1
    while q6_1_signal == 1:
        # noinspection PyBroadException
        try:
            resp_fantasy = float(input())
            q6_1_signal = 0
        except:
            print("Invalid response. Please type 0 or 1 (0=no, 1=yes)")
    if resp_fantasy == 0:
        pass
    else:
        comp_score = comp_score + 10
        print("That's great! My team name for this season is OJ's Lawyers LOL!")

# determine if they'd make a good friend
# if they answered yes to 4/6 or more, we are friends
if comp_score >= (max_points/2):
    print("Based on our compatibility score, we should be friends!")
else:
    print("Based on our compatibility score, we wouldn't make very good friends.")
percent = comp_score/max_points * 100
print("You scored", comp_score, "points out of a possible", max_points, "points.")
print("We are {:03.2f} percent compatible to be friends!".format(percent))
