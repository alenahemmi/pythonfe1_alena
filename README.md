# Python for Everyone: Alena's Repository

This repo holds my code used in Python for Everyone taught at Carlow University in Fall 2023

## Project 1: Road trip computer

This project demonstrates variable names and user input

## Project 2: Might we be friends

Exploration of the power of nested conditional logic
Includes factors of while, try, and except

## 
